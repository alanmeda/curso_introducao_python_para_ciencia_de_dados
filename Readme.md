# Bem-vindo ao Curso


### Pós-graduação em ciência de dados e machine learning

#### Módulo 2 - Gestão de dados

#### Disciplina: **Introdução a Python para Ciência de Dados**

#### Turma: **B**

#### Professor: Professor MSc. Ricardo José Menezes Maia.  http://lattes.cnpq.br/0706885145380777
* Doutorado, Universidade de Brasília UNB - Ciência da Computação - 2019 - em andamento
* Mestrado, Universidade de São Paulo USP - Escola Politécnica, Engenharia de Computação - 2008 a 2010
* Bacharelado em Ciência da Computação, Universidade Federal do Amazonas - 1999 a 2003
* Técnico em Programação, Fundação de Ensino e Pesquisa Matias Machline - Sharp do Brasil - 1992 a 1994.

Desejo que nossa experiência neste curso seja a melhor possível!

Abaixo está o sumário do curso e para baixar todo conteúdo que está versionado no git clique no link
https://gitlab.com/ricardo.jmm/curso_introducao_python_para_ciencia_de_dados/-/archive/master/curso_introducao_python_para_ciencia_de_dados-master.zip

Após isso descompacte o arquivo em uma pasta da sua escolha

Abra o jupyter notebook. Lembrando que há inúmeras formas de fazer. Exemplo no linux e macos teria uma das formas #jupyter notebook

Após abrir o jupyter você irá clicar em File->Open e escolher o notebook "Bem vindo ao curso de Introdução a Python para Ciência de Dados!.ipynb" que estará na raiz da pasta. 

Este jupyter terá o sumário para você navegar entre os assuntos.

Neste curso os exercícios que estarão na pasta exercícios sempre solicitados que sejam entregues.

Plano de aula está no slide IntroduçãoAPythonParaCiênciaDeDados.pptx
[Introdução a Python para Ciência de Dados.pptx](IntroducaoPythonParaCienciaDeDados.pptx)

Plano de Ensino está no arquivo PlanodeEnsino-CursodeCienciadeDadoseMachineLearning-IntroducaoPythonParaCienciaDeDados.docx
[Plano de Ensino](PlanodeEnsino-CursodeCienciadeDadoseMachineLearning-IntroducaoPythonParaCienciaDeDados.docx)

Será feita 1 avaliação que serão disponibilizadas no https://classroom.google.com. A entrega será para dia 06/04/2020.

A proposta de ensino é:
* 07/03/2020 - 1 - Curso Introdutório de Python
* 14/03/2020 - 2 - Python para análise de dados
* 21/03/2020 - 3 - Python para visualização de dados

* Referências
* * "Por que todos deveriam aprender a programar?" [https://youtu.be/mHW1Hsqlp6A](https://youtu.be/mHW1Hsqlp6A)
* * "Python Para Análise de Dados: Tratamento de Dados com Pandas, NumPy e IPython" - ISBN-10: 8575226479
* * https://docs.python.org/3/tutorial/
* * https://numpy.org
* * https://www.scipy.org/scipylib/
* * https://pandas.pydata.org
* * https://matplotlib.org
* * https://seaborn.pydata.org/
* * http://dados.gov.br
* * https://www.kaggle.com
* * https://www.usa.gov/developer
* * https://dadosabertos.camara.leg.br
* * https://dadosabertos.bcb.gov.br
* * https://www12.senado.leg.br/dados-abertos
* * http://dados.gov.br/organization/caixa-economica-federal-cef
* * https://www.aneel.gov.br/dados
* * http://www.ipea.gov.br/acessoainformacao/dados-abertos
* * http://dadosabertos.ibama.gov.br/organization/instituto-brasileiro-do-meio-ambiente-e-dos-recursos-naturais-renovaveis
* * http://dadosabertos.mec.gov.br
* * http://dados.gov.br/organization/ministerio-da-justica-e-seguranca-publica-mj
* * https://github.com/fivethirtyeight/data
* * https://datahub.io/collections/football#football-datasets-on-datahub
* * http://www.portaldatransparencia.gov.br
* * https://www.reddit.com/r/datasets/
* * https://github.com/BuzzFeedNews
* * https://meta.wikimedia.org/wiki/Mirroring_Wikimedia_project_XML_dumps#Media0
* * https://dumps.wikimedia.org
* * http://www.tse.jus.br/eleicoes/estatisticas/repositorio-de-dados-eleitorais-1/repositorio-de-dados-eleitorais
* * https://acessoainformacao.ibge.gov.br/acesso-a-informacao/dados-abertos.html


## Sumário:
* [1 - Curso Introdutório de Python](1_CursoIntrodutorioPython)
* * [1.1 - Objetos em python e estruturas dados](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados)
* * * [1.1.1 - Objetos e estruturas de dados básicas](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.1_ObjetosEstruturasDadosBasicas)
* * * * [1.1.1.1 - Atribuição de Variáveis.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.1_ObjetosEstruturasDadosBasicas/1.1.1.1-AtribuicaoVariaveis.ipynb)
* * * * [1.1.1.2 - Números.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.1_ObjetosEstruturasDadosBasicas/1.1.1.2-Numeros.ipynb)
* * * * [1.1.1.3 - Strings.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.1_ObjetosEstruturasDadosBasicas/1.1.1.3-Strings.ipynb)
* * * * [1.1.1.4 - Print Formatando Strings.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.1_ObjetosEstruturasDadosBasicas/1.1.1.4-PrintFormatandoStrings.ipynb)
* * * * [1.1.1.5 - Listas.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.1_ObjetosEstruturasDadosBasicas/1.1.1.5-Listas.ipynb)
* * * * [1.1.1.6 - Dicionários.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.1_ObjetosEstruturasDadosBasicas/1.1.1.6-Dicionarios.ipynb)
* * * * [1.1.1.7 - Tuplas.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.1_ObjetosEstruturasDadosBasicas/1.1.1.7-Tuplas.ipynb)
* * * * [1.1.1.8 - Conjuntos e tipos Booleanos.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.1_ObjetosEstruturasDadosBasicas/1.1.1.8-Conjuntos_E_Booleans.ipynb)
* * * * [1.1.1.9 - Arquivos.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.1_ObjetosEstruturasDadosBasicas/1.1.1.9-Arquivos.ipynb)
* * * [1.1.2 - Avançado - Objetos e Estruturas de Dados Básicas](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.3_Exercicios)
* * * * [1.1.2.1-Numeros.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.2_AvancadoObjetosEstruturasDadosBasicas/1.1.2.1-Numeros.ipynb)
* * * * [1.1.2.2-Strings.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.2_AvancadoObjetosEstruturasDadosBasicas/1.1.2.2-Strings.ipynb)
* * * * [1.1.2.3-Conjuntos.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.2_AvancadoObjetosEstruturasDadosBasicas/1.1.2.3-Conjuntos.ipynb)
* * * * [1.1.2.4-Listas.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.2_AvancadoObjetosEstruturasDadosBasicas/1.1.2.4-Listas.ipynb)
* * * * [1.1.2.4-Dicionários Avançados.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.2_AvancadoObjetosEstruturasDadosBasicas/1.1.2.5-DicionariosAvancados.ipynb)
* * * [1.1.3_Exercícios](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.3_Exercicios/)
* * * * [1.1.3.1-Exercício sobre Objetos.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.3_Exercicios/1.1.3.1-ExercicioObjetos.ipynb)
* * * * [1.1.3.2-Exercício sobre  Objetos e Estruturas de Dados.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.3_Exercicios/1.1.3.2-ExercicioObjetosEstruturaDados.ipynb)
* * * * [1.1.3.3-Exercícios.ipynb](1_CursoIntrodutorioPython/1.1_ObjetosPythonEstruturasDados/1.1.3_Exercicios/1.1.3.3-Exercicios.ipynb)
* * [1.2 - Estruturas Condicionais e estruturas de Repetição](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao)
* * * [1.2.1_Operadores para Comparação](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.1_OperadoresComparacao)
* * * * [1.2.1.1-Operadores para Comparação.ipynb](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.1_OperadoresComparacao/1.2.1.1-OperadoresComparacao.ipynb)
* * * * [1.2.1.2-Operadores De Comparação Encadeados.ipynb](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.1_OperadoresComparacao/1.2.1.2-OperadoresDeComparaçãoEncadeados.ipynb)
* * * [1.2.2_Estruturas Condicionais Repetição](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.2_EstruturasCondicionaisRepeticao)
* * * * [1.2.2.1-Introdução.ipynb](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.2_EstruturasCondicionaisRepeticao/1.2.2.1-Introducao.ipynb)
* * * * [1.2.2.2-if, elif, else.ipynb](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.2_EstruturasCondicionaisRepeticao/1.2.2.2-if_elif_else.ipynb)
* * * * [1.2.2.3-for.ipynb](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.2_EstruturasCondicionaisRepeticao/1.2.2.3-for_lacos.ipynb)
* * * * [1.2.2.4-while.ipynb](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.2_EstruturasCondicionaisRepeticao/1.2.2.4-while_lacos.ipynb)
* * * * [1.2.2.5-Operadores Úteis.ipynb](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.2_EstruturasCondicionaisRepeticao/1.2.2.5-OperadoresUteis.ipynb)
* * * * [1.2.2.6-Listas.ipynb](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.2_EstruturasCondicionaisRepeticao/1.2.2.6-Listas.ipynb)
* * * [1.2.3_Exercícios](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.3_Exercicios)
* * * * [1.2.3.1-Exercicios.ipynb](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.3_Exercicios/1.2.3.1-Exercicios.ipynb)
* * * * [1.2.3.2-Exercícios.ipynb](1_CursoIntrodutorioPython/1.2_EstruturasCondicionaisRepeticao/1.2.3_Exercicios/1.2.3.1-Exercicios.ipynb)
* * [1.3 - Funções](1_CursoIntrodutorioPython/1.3_Funcoes)
* * * [1.3.1_MetodosFuncoes](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.1_MetodosFuncoes)
* * * * [1.3.1.1-Metodos.ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.1_MetodosFuncoes/1.3.1.1-Metodos.ipynb)
* * * * [1.3.1.2-Funcoes.ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.1_MetodosFuncoes/1.3.1.2-Funcoes.ipynb)
* * * * [1.3.1.3-Expressoes_Lambda_Map_Filter.ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.1_MetodosFuncoes/1.3.1.3-ExpressoesLambdaMapFilter.ipynb)
* * * * [1.3.1.4-DeclaraçõesEscopoAninhados.ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.1_MetodosFuncoes/1.3.1.4-DeclaracoesEscopoAninhados.ipynb)
* * * * [1.3.1.5-args_kwargs.ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.1_MetodosFuncoes/1.3.1.5-ArgsKwargs.ipynb)
* * * [1.3.2_Built-inFunctions](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.2_Built-inFunctions)
* * * * [1.3.2.1-Filter.ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.2_Built-inFunctions/1.3.2.1-Filter.ipynb)
* * * * [1.3.2.2-Zip.ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.2_Built-inFunctions/1.3.2.2-Zip.ipynb)
* * * * [1.3.2.3-Enumerate.ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.2_Built-inFunctions/1.3.2.3-Enumerate.ipynb)
* * * * [1.3.2.4-all()_any().ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.2_Built-inFunctions/1.3.2.4-all()_any().ipynb)
* * * * [1.3.2.5-Complex.ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.2_Built-inFunctions/1.3.2.5-Complex.ipynb)
* * * * [1.3.2.6-Map.ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.2_Built-inFunctions/1.3.2.6-Map.ipynb)
* * * * [1.3.2.7-Reduce.ipynb](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.2_Built-inFunctions/1.3.2.7-Reduce.ipynb)
* * * [1.3.3_PythonDecorators](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.3_PythonDecorators)
* * * [1.3.4_PythonGenerators](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.4_PythonGenerators)
* * * [1.3.5_Exercicios](1_CursoIntrodutorioPython/1.3_Funcoes/1.3.5_Exercicios)
* * [1.4 - Módulos](1_CursoIntrodutorioPython/1.4_Modulos)
* * [1.5 - Erros e Exceções](1_CursoIntrodutorioPython/1.5_ErrosExcecoes)
* * [1.6 - Programação Orientada a Objetos](1_CursoIntrodutorioPython/1.6_POO)
* * [1.7 - Processamento Paralelo](1_CursoIntrodutorioPython/1.7_ProcessamentoParalelo)
* * * [1.7.1_ProcessamentoParalelo](1_CursoIntrodutorioPython/1.7_ProcessamentoParalelo/1.7.1_ProcessamentoParalelo.ipynb)
* [2 - Python para análise de dados](2_PythonParaAnaliseDeDados)
* * [2.1 - NumPy](2_PythonParaAnaliseDeDados/2.1_NumPy)
* * * [2.1.1_NumPy Arrays.ipynb](2_PythonParaAnaliseDeDados/2.1_NumPy/2.1.1_NumPyArrays.ipynb)
* * * [2.1.2 - Operações em NumPy](2_PythonParaAnaliseDeDados/2.1_NumPy/2.1.2_OperacoesNumPy.ipynb)
* * * [2.1.3 - Indexação de Numpys e Arrays](2_PythonParaAnaliseDeDados/2.1_NumPy/2.1.3_IndexacaoNumpysArrays.ipynb)
* * * [2.1.4_Exercicios](2_PythonParaAnaliseDeDados/2.1_NumPy/2.1.4_Exercicios)
* * * * [2.1.4.1_ExerciciosNumpy.ipynb](2_PythonParaAnaliseDeDados/2.1_NumPy/2.1.4_Exercicios/2.1.4.1_ExerciciosNumpy.ipynb)
* * [2.2 - Pandas](2_PythonParaAnaliseDeDados/2.2_Pandas)
* * * [2.2.2_Series](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.2_Series.ipynb)
* * * [2.2.3_DataFrames](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.3_DataFrames.ipynb)
* * * [2.2.4_DadosAusentes](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.4_DadosAusentes.ipynb)
* * * [2.2.5_GroupBy](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.5_GroupBy.ipynb)
* * * [2.2.6_MesclarJuntarConcatenar](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.6_MesclarJuntarConcatenar.ipynb)
* * * [2.2.7_Operacoes](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.7_Operacoes.ipynb)
* * * [2.2.8_EntradaSaidaDeDados](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.8_EntradaSaidaDeDados.ipynb)
* * * [2.2.9_Exercícios](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.9_Exercicios)
* * * * [2.2.1.1_Exercícios_DadosAbertos_Índice_Geral_de_Reclamações_IGR.ipynb](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.9_Exercicios/2.2.9.1_ExerciciosIndiceGeraldeReclamacoes_IGR.ipynb)
* * * * [2.2.1.2_Exercicios_ComprasdeEcomerceipynb](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.9_Exercicios/2.2.9.2_ExerciciosComprasdeEcomerce.ipynb)
* * * * [2.2.1.3_ExercíciosSalários.ipynb](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.9_Exercicios/2.2.9.3_ExerciciosSalarios.ipynb)
* * * * [2.2.1.4_DadosAbertos_TA_PRECO_MEDICAMENTO.ipynb](2_PythonParaAnaliseDeDados/2.2_Pandas/2.2.9_Exercicios/2.2.9.4_TA_PRECO_MEDICAMENTO.ipynb)
* * [2.3 - SciPy](2_PythonParaAnaliseDeDados/2.3_SciPy)
* * * [2.3.1 - SciPy.ipynb](2_PythonParaAnaliseDeDados/2.3_SciPy/2.3.1_SciPy.ipynb)
* [3 - Python para visualização de dados](3_PythonParaVisualizacaoDeDados)
* * [3.1 - Visualização de Dados Incorporada do Pandas](3_PythonParaVisualizacaoDeDados/3.1_VisualizacaoDeDadosIncorporadaDoPandas)
* * * [3.1.1_Visualização_de_dados_incorporada_do_Pandas.ipynb](3_PythonParaVisualizacaoDeDados/3.1_VisualizacaoDeDadosIncorporadaDoPandas/3.1.1_Visualização_de_dados_incorporada_do_Pandas.ipynb)
* * [3.2_Seaborn](3_PythonParaVisualizacaoDeDados/3.2_Seaborn)
* * * [3.2.1_PairGrid.ipynb](3_PythonParaVisualizacaoDeDados/3.2_Seaborn/3.2.1_PairGrid.ipynb)
* * * [3.2.2_PlotsCategóricos.ipynb](3_PythonParaVisualizacaoDeDados/3.2_Seaborn/3.2.2_PlotsCategóricos.ipynb)
* * * [3.2.3_PlotsMatriciais.ipynb](3_PythonParaVisualizacaoDeDados/3.2_Seaborn/3.2.3_PlotsMatriciais.ipynb)
* * * [3.2.4_PlotsDeDistribuições.ipynb](3_PythonParaVisualizacaoDeDados/3.2_Seaborn/3.2.4_PlotsDeDistribuições.ipynb)
* * * [3.2.5_PlotsDeRegressões.ipynb](3_PythonParaVisualizacaoDeDados/3.2_Seaborn/3.2.5_PlotsDeRegressões.ipynb)
* * * [3.2.6_EstilosCores.ipynb](3_PythonParaVisualizacaoDeDados/3.2_Seaborn/3.2.6_EstilosCores.ipynb)
* * [3.3_Matplotlib](3_PythonParaVisualizacaoDeDados/3.4_Matplotlib)
* * * [3.3.1_ResumoMatplotlib.ipynb](3_PythonParaVisualizacaoDeDados/3.4_Matplotlib/3.4.1_ResumoMatplotlib.ipynb)
* * * [3.3.2_ConceitosAvançadosEmMatplotLib.ipynb](3_PythonParaVisualizacaoDeDados/3.4_Matplotlib/3.4.2_ConceitosAvançadosEmMatplotLib.ipynb)
